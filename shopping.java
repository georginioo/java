import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

//alot of system.out.println...

class Main {
  public static void main(String[] args) {
    //How could this be laid out better?
    System.out.println("Welcome to Poundland");

    Cart cart = new Cart();
    ArrayList<String> thelist = cart.arraylist;
    String[] fruit = cart.fruitList;
    String[] drink = cart.drinkList;
    String[] cereal = cart.cerealList;
    int[] amountOfProducts = cart.amountOfProducts;
    Checkout checkout = new Checkout(thelist,fruit,drink,cereal,amountOfProducts);
    double totalPrice = checkout.totalPrice;
    double[] prices = checkout.prices;
    Receipt receipt = new Receipt(totalPrice,thelist,amountOfProducts,prices);
  }
}

class Cart
{
  ArrayList<String> arraylist;
  String[] fruitList;
  String[] drinkList;
  String[] cerealList;
  int[] amountOfProducts;

  public Cart(){
    Products product = new Products();
    arraylist = product.getProductArrayList();
    fruitList = product.fruit;
    drinkList = product.drink;
    cerealList = product.cereal;
    amountOfProducts = product.amountOfProducts;
  }
}

class Checkout
{
  double totalPrice = 0;
  double[] prices = {3.0,2.0,1.5};

  public Checkout(ArrayList<String> array_list, String[] fruitList, String[] drinkList, String[] cerealList, int[] amountOfProducts)
  {
    for (int i = 0; i < amountOfProducts.length; i++)
    {
      totalPrice += prices[i] * amountOfProducts[i];
    }
  }
}

class Receipt
{
  public Receipt(double totalPrice, ArrayList<String> array_list, int[] amountOfProducts, double[] priceList)
  {
    System.out.println("\nPoundland\n");
    for (int i = 0; i < array_list.size(); i++)
    {
      int priceIndex;
      if (i < amountOfProducts[0]) { priceIndex = 0; }
      else if (i < amountOfProducts[0] + amountOfProducts[1]) { priceIndex = 1; }
      else { priceIndex = 2; }
      System.out.println("�"+priceList[priceIndex] + "   "+array_list.get(i));
    }
    System.out.println("\nTotal Price: �"+totalPrice);
  }
}

class Products
{
  boolean finishedShopping = false;
  String[] productArray = new String[0];
  ArrayList<String> boughtProductList = new ArrayList<String>(Arrays.asList(productArray));

  String[] fruit = {"Apple", "Banana","Orange","Strawberry","Kiwi"};
  String[] drink = {"Milk","Tea","Cola","Fanta","Water"};
  String[] cereal = {"Weetabix","Shreddies","Cornflakes","Coco Pops","Porridge"};
  int[] amountOfProducts = {0,0,0};
  Scanner scan = new Scanner(System.in);

  // Constructer
  public Products()
  {
    chooseProductsToDisplay();
  }

  // Choose which list of products you would like to see
  public void chooseProductsToDisplay()
  {
    System.out.println("\nWe have three types of products: Fruit, Drinks and Cereal.");
    System.out.println("Which would you like to see? F/D/C/X to finish shopping.");
    boolean validEntry = false;
    while (!validEntry)
    {
      char input = scan.next().charAt(0);
      char inputUpper = Character.toUpperCase(input);
      switch (inputUpper)
      {
        case 'F':
          showMessage("fruit");
          validEntry = displayProduct(fruit);
          chooseProduct(fruit,0);
          break;
        case 'D':
          showMessage("drinks");
          validEntry = displayProduct(drink);
          chooseProduct(drink,1);
          break;
        case 'C':
          showMessage("cereal");
          validEntry = displayProduct(cereal);
          chooseProduct(cereal,2);
          break;
        case 'X':
          if (boughtProductList.size() == 0) { System.out.println("\nYou have not bought anything, goodbye!"); System.exit(0); }
          else
          {
            finishedShopping = true;
            return;
          }
          break;
        default:
          System.out.println("\nInvalid Entry. Please enter F, C, D or X to finish shopping.");
          validEntry = false;
          break;
      }
    }
  }

  // Display a list of products
  public boolean displayProduct(String[] productList)
  {
    boolean validEntry;

    for (int i = 0; i < productList.length; i++)
    {
      System.out.print(i + ": "+ productList[i]+"\n");
    }
    validEntry = true;
    return validEntry;
  }

  public void showMessage(String message)
  {
    System.out.println("\nHere are the "+message+" available: ");
  }

  public void chooseProduct(String[] productList,int productType)
  {
    // An array that has each product purchased
    System.out.print("Would you like to select one of these items? Y/N  ");
    boolean validEntry = false;
    while (!validEntry)
    {
      char input = scan.next().charAt(0);
      input = Character.toUpperCase(input);
      if (input == 'Y') 
      {
        boolean wantToPurchase = true;
        while (wantToPurchase)
        {
          System.out.print("Please enter the number that corresponds to the item you want to buy. ");
          
          // Ensure that the input does correspond to one of the items in the list.
          int inputInt = productList.length+1;

          boolean valid = false;
          while (!valid)
          {
            String input_ = scan.nextLine();

            try
            {
              inputInt = Integer.parseInt(input_);
              valid = true;
            }
            catch (NumberFormatException ex)
            {
              if (input_.length() > 0)
              {
                System.out.println("\nThat is not a valid option. Please pick one of the numbers shown above.");
              }
              valid = false;
            }
          }

          while (inputInt >= productList.length)
          {
            if (inputInt >= productList.length) 
            { 
              System.out.println("That number does not represent an item. Please enter one of the numbers shown."); 
              inputInt = scan.nextInt();
            }
          }
          // End of validation
          
          String item = productList[inputInt];
          System.out.print("How many "+item+"s would you like to buy? ");
          int numberDesired = 1;

          valid = false;
          while (!valid)
          {
            String input_string = scan.nextLine();
            try
            {
              numberDesired = Integer.parseInt(input_string);
              if (numberDesired < 1000 && numberDesired > 0) { valid = true; }
              else if (numberDesired <= 0) { System.out.println("Invalid Entry. Please enter a number above 0."); }
              else { System.out.println("That number is too large. Please pick a number below 1000."); }
            }
            catch (NumberFormatException ex)
            {
              System.out.println("Invalid Entry. Please enter a NUMBER.");
            }
          }
          // Add to number of fruits/drinks/cereals purchased
          addToAmountOfProducts(productType,numberDesired);

          for (int i = 0; i < numberDesired; i++)
          {
            boughtProductList.add(item);
          }
          valid = false;
          while (!valid)
          {
            valid = true;
            System.out.println("Would you like to purchase any other product from this list? Y/N ");
            input = scan.next().charAt(0);
            input = Character.toUpperCase(input);
            if (input == 'Y')
            {
              wantToPurchase = true;
            }
            else if (input == 'N')
            {
              wantToPurchase = false;
              chooseProductsToDisplay();
            }
            else { System.out.println("Invalid Entry. Please enter 'Y' or 'N'."); valid = false; }
          }
        }
        validEntry = true;
      }
      else
      {
        if (input == 'N') { chooseProductsToDisplay(); validEntry = true; }
        else { System.out.println("Invalid entry. Enter 'Y' or 'N'."); }
      }
    }
  }

  // Add the desired amount of products to the amountOfProducts array (0, 0, 3) = (F, D, C)
  public void addToAmountOfProducts(int index, int amountToAdd)
  {
    amountOfProducts[index] = amountOfProducts[index] + amountToAdd;
  }

  public ArrayList<String> getProductArrayList()
  {
    return boughtProductList;
  }
}