import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.lang.Character;

class Main
{
  static ArrayList<Integer> letterPositions = new ArrayList<Integer>();

  public static void main(String[] args)
  {
    int timeInSecs = (int)System.currentTimeMillis() / 1000;
    String[] words = {"APPLE","BANANA","ORANGE","PINEAPPLE"};
    String alphabet = "abcdefghijklmnopqrstuvwxyz";
    String chosenWord = words[timeInSecs%words.length];
    System.out.println(chosenWord);
    char[] chosenWordArray = words[timeInSecs%words.length].toCharArray();
    char[] displayWordArray = new char[chosenWordArray.length];
    for (int i = 0; i < displayWordArray.length; i++)
    {
      displayWordArray[i] = '*';
    }
    String displayWord = new String(displayWordArray);
    System.out.println(displayWord);
    boolean guessed = false;
    while (!guessed)
    {
      Scanner scanner = new Scanner(System.in);
      char input = scanner.next().charAt(0);
      input = Character.toUpperCase(input);
      if (isCharInArray(input,chosenWordArray))
      { 
        for (int i = 0; i < letterPositions.size(); i++)
        {
          displayWordArray[letterPositions.get(i)] = input;
        }
        letterPositions.clear();
        displayWord = new String(displayWordArray);
      } 
      else 
      { 
        System.out.println("incorrect"); 
      }
      System.out.println(displayWord);
      if (isWordGuessed(displayWordArray)) { guessed = true; }
    }
    System.out.println("Well done, you have guessed the word!");
  }

  // Finds if a given character is in a given array
  // Also stores all the positions in which that character exists in the letterPositions array
  private static boolean isCharInArray(char character, char[] array)
  {
    boolean isInArray = false;
    for (int i = 0; i < array.length; i++)
    {
      if (array[i] == character) { isInArray = true; letterPositions.add(i); }
    }
    return isInArray;
  }

  // Check to see if the entire word has been guessed
  private static boolean isWordGuessed(char[] word)
  {
    boolean guessed = true;
    for (int i = 0; i < word.length; i++)
    {
      if (word[i] == '*') { guessed = false; }
    }
    return guessed;
  }
}